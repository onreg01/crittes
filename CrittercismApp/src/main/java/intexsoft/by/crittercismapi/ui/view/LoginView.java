package intexsoft.by.crittercismapi.ui.view;

/**
 * Created by anastasya.konovalova on 21.07.2014.
 */
public interface LoginView extends BaseView
{
	void onFillStoredFields(String login, String password);

	void showProgressBar();

	void hideProgressBar();

	boolean isFromLogout();
}
