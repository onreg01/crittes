package intexsoft.by.crittercismapi.ui.presenter;

import intexsoft.by.crittercismapi.ui.view.MainView;

/**
 * Created by anastasya.konovalova on 22.06.2014.
 */
public interface MainPresenter extends BasePresenter<MainView>
{

}
