package intexsoft.by.crittercismapi.ui.fragment;

/**
 * Created by eugene.galonsky on 28.03.14.
 */
public interface NavigationDrawerCloser
{
	void closeDrawer();
}
