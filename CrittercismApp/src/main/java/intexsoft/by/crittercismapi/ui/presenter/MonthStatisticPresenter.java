package intexsoft.by.crittercismapi.ui.presenter;

import intexsoft.by.crittercismapi.ui.view.MonthStatisticsView;

/**
 * Created by vadim on 07.10.2014.
 */
public interface MonthStatisticPresenter extends BasePresenter<MonthStatisticsView> {
}
