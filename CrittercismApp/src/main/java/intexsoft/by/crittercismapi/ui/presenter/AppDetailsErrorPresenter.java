package intexsoft.by.crittercismapi.ui.presenter;

import intexsoft.by.crittercismapi.ui.view.AppDetailsErrorView;

/**
 * Created by Евгений on 04.08.2014.
 */
public interface AppDetailsErrorPresenter extends BasePresenter<AppDetailsErrorView>
{

}
