package intexsoft.by.crittercismapi.ui.view;

import android.app.Activity;

/**
 * Created by anastasya.konovalova on 21.07.2014.
 */
public interface BaseView
{
	Activity getContainer();
}
