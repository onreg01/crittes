package intexsoft.by.crittercismapi.ui.view.animation;

import android.view.animation.Animation;

/**
 * Created by Евгений on 26.08.2014.
 */
public abstract class EndAnimationListener implements Animation.AnimationListener
{
	@Override
	public void onAnimationStart(Animation animation)
	{

	}

	@Override
	public void onAnimationRepeat(Animation animation)
	{

	}
}
