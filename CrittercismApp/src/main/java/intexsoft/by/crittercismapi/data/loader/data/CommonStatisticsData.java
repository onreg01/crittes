package intexsoft.by.crittercismapi.data.loader.data;

/**
 * Created by anastasya.konovalova on 27.08.2014.
 */
public class CommonStatisticsData
{
	private String mostCrashesByMonthAppId;

	private String mostCrashesByMonthAppName;
	private String mostCrashesByNightAppName;
	private String mostCrashesByAllTimeAppName;

	private String mostErrorByAllTimeAppName;
	private String mostErrorByMonthAppName;
	private String mostErrorByNightAppName;

	private String mostDownloadsByAllTimeAppName;
	private String mostDownloadsByMonthAppName;
	private String mostDownloadsByNightAppName;

	public String getMostErrorByAllTimeAppName()
	{
		return mostErrorByAllTimeAppName;
	}

	public void setMostErrorByAllTimeAppName(String mostErrorByAllTimeAppName)
	{
		this.mostErrorByAllTimeAppName = mostErrorByAllTimeAppName;
	}

	public String getMostErrorByMonthAppName()
	{
		return mostErrorByMonthAppName;
	}

	public void setMostErrorByMonthAppName(String mostErrorByMonthAppName)
	{
		this.mostErrorByMonthAppName = mostErrorByMonthAppName;
	}

	public String getMostErrorByNightAppName()
	{
		return mostErrorByNightAppName;
	}

	public void setMostErrorByNightAppName(String mostErrorByNightAppName)
	{
		this.mostErrorByNightAppName = mostErrorByNightAppName;
	}

	public String getMostDownloadsByAllTimeAppName()
	{
		return mostDownloadsByAllTimeAppName;
	}

	public void setMostDownloadsByAllTimeAppName(String mostDownloadsByAllTimeAppName)
	{
		this.mostDownloadsByAllTimeAppName = mostDownloadsByAllTimeAppName;
	}

	public String getMostDownloadsByMonthAppName()
	{
		return mostDownloadsByMonthAppName;
	}

	public void setMostDownloadsByMonthAppName(String mostDownloadsByMonthAppName)
	{
		this.mostDownloadsByMonthAppName = mostDownloadsByMonthAppName;
	}

	public String getMostDownloadsByNightAppName()
	{
		return mostDownloadsByNightAppName;
	}

	public void setMostDownloadsByNightAppName(String mostDownloadsByNightAppName)
	{
		this.mostDownloadsByNightAppName = mostDownloadsByNightAppName;
	}

	public String getMostCrashesByAllTimeAppName()
	{
		return mostCrashesByAllTimeAppName;
	}

	public void setMostCrashesByAllTimeAppName(String mostCrashesByAllTimeAppName)
	{
		this.mostCrashesByAllTimeAppName = mostCrashesByAllTimeAppName;
	}

	public String getMostCrashesByNightAppName()
	{
		return mostCrashesByNightAppName;
	}

	public void setMostCrashesByNightAppName(String mostCrashesByNightAppName)
	{
		this.mostCrashesByNightAppName = mostCrashesByNightAppName;
	}


	public String getMostCrashesByMonthAppId()
	{
		return mostCrashesByMonthAppId;
	}

	public void setMostCrashesByMonthAppId(String mostCrashesByMonthAppId)
	{
		this.mostCrashesByMonthAppId = mostCrashesByMonthAppId;
	}

	public String getMostCrashesByMonthAppName()
	{
		return mostCrashesByMonthAppName;
	}

	public void setMostCrashesByMonthAppName(String mostCrashesByMonthAppName)
	{
		this.mostCrashesByMonthAppName = mostCrashesByMonthAppName;
	}
}
