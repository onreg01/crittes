package intexsoft.by.crittercismapi.data.remote.request;

/**
 * Created by Евгений on 29.07.2014.
 */
public class PieRequest
{

	PieRequestInternal params;

	public PieRequestInternal getParams()
	{
		return params;
	}

	public void setParams(PieRequestInternal params)
	{
		this.params = params;
	}
}
